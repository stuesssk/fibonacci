ASFLAGS += -W
CFLAGS += -O1 -masm=intel -fno-asynchronous-unwind-tables

BIN=fibonacci
DEPS=fibonacci.o

$(BIN) : $(DEPS)

.PHONY: clean

clean:
	$(RM) $(BIN) $(DEPS)
