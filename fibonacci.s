.intel_syntax noprefix

UsageStatement:
    .asciz "Usage: %s <#0-370>\n"
FibFourStr:
    .asciz "Fibonacci Number: 0x%lx%lx%lx%lx\n"
FibThreeStr:
    .asciz "Fibonacci Number: 0x%lx%lx%lx\n"
FibTwoStr:
    .asciz "Fibonacci Number: 0x%lx%lx\n"
FibOneStr:
    .asciz "Fibonacci Number: 0x%lx\n"
EnterString:
    .asciz "Enter Fibonacci Squence Number(0-370)>"
InputString:
    .string "%s"

.globl main
main:
    # Zero aligning the stack
    sub rsp, 24
    cmp rdi, 2
    mov rbp, [rsi]
    # More than 1 command line argument after the file name
    jg Usage
    mov rdi, QWORD PTR[rsi + 8]
    # Skip input if user supplied number on the command line
    je StringConv

Input:
    # Get user input if not in command line
    mov rdi, OFFSET EnterString
    call printf
    mov rdi, stdin
    mov rsi, OFFSET InputString
    # Setting up stack space for string-- Source Liam and S. Cuillo
    lea rdx, [rsp + 8]
    call fscanf
    # Load into rdi from stack to convert string into number
    lea rdi, [rsp + 8]

StringConv:
    #Converting number recieved into a integer
    mov rdx, 10
    lea rsi, [rsp]
    call strtol
    mov rcx, [rsp]
    # rcx will only point to null if strtol is successful-- Source Liam
    cmp BYTE PTR[rcx], 0x00
    # If pointing to anything but null, there was an  invalid user input
    jne Usage

    # Checking to ensure the number received is within the valid range
    cmp eax, 0
    # Checking for negative numbers
    jl Usage
    # Ensure number is not greater than upper limit of four registers
    cmp eax, 370
    jg Usage

    mov ecx, eax

    # Zero all the registers we need
    xor rsi, rsi
    xor rdi, rdi
    xor rbx, rbx
    xor rdx, rdx
    xor r11, r11
    xor r12, r12
    xor r14, r14
    #intialize to one to start sequence F(1) = 1
    mov rax, 1

    
Fibonacci:
    # if ecx is zero we have reached the sequence number the user input
    test ecx, ecx
    je Output

    # Swap values to preserve the previous value
    xchg rdi, rax # Lowest bits in output
    xchg r11, r14
    xchg rdx, r12
    xchg rsi, rbx  # Highest bits in output

    # Calculating the next fibonacci number
    add rdi, rax # Lowest bits in output
    adc r14, r11
    adc rdx, r12
    adc rsi, rbx  # Highest bits in output
    
    # decrement the counter, break when it hits zero
    dec ecx
    jne Fibonacci

Output:
    # Testing to see if leading zeros need to be striped
    test rsi, rsi
    jne PrintFour
    test rdx, rdx
    jne PrintThree
    test r14, r14
    jne PrintTwo

    # Output only fills one register
    mov rsi, rdi
    mov rdi, OFFSET FibOneStr
    call printf
    add rsp, 24
    ret

PrintFour:
    # Using all four registers for output
    # Move the numbers to proper registers to set up printf
    # rsi is already set to being most significant bits &
    # rdi is already set to being 2nd most significant bits with
    # four registers
    mov rcx, r14
    mov r8, rdi
    mov rdi, OFFSET FibFourStr
    call printf
    add rsp, 24
    ret
PrintThree:
    # Using Three registers for output
    mov rsi, rdx
    mov rdx, r14
    mov rcx, rdi
    mov rdi, OFFSET FibThreeStr
    call printf
    add rsp, 24
    ret
PrintTwo:
    #using Two registers for output
    mov rsi, r14
    mov rdx, rdi
    mov rdi, OFFSET FibTwoStr
    call printf
    add rsp, 24
    ret

Usage:
    # Error message. Input was invalid
    mov rdi, OFFSET UsageStatement
    mov rsi, rbp
    call printf
    add rsp, 24
    mov eax, 1
    ret
